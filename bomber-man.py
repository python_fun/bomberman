__author__ = 'Alan'
import pygame,glob
from pygame.locals import QUIT, KEYDOWN,KEYUP, K_SPACE, K_LEFT, K_RIGHT, K_UP, K_DOWN, K_a, K_w, K_s, K_d, K_b, K_n, K_INSERT, K_DELETE, Rect
pygame.init()
screenSize = (1140, 702) 
telaBlitSize = (800, 600)
screen = pygame.display.set_mode(screenSize, 0, 32)
clock = pygame.time.Clock()
FPS = 30.0
#------------------- Cenarios -------------------------
cenario = [
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 2, 2, 0, 2, 0, 0, 0, 2, 0, 1],
            [1, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 2, 0, 0, 1],
            [1, 2, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 2, 0, 2, 0, 2, 2, 2, 2, 2, 0, 0, 2, 0, 1],
            [1, 0, 0, 0, 2, 2, 2, 2, 2, 2, 0, 2, 0, 0, 1],
            [1, 0, 1, 0, 1, 2, 1, 0, 1, 2, 1, 0, 1, 0, 1],
            [1, 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 0, 1],
            [1, 2, 0, 2, 0, 0, 0, 0, 2, 2, 0, 2, 0, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 2, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 2, 2, 2, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
          ]


#player 1 comandos:
    #direcionais movimentam o personagem
    # tecla insert aciona a bomba
#player 2 comandos:
    # w,a,s,d movimentam o personagem
    # tecla b aciona a bomba




#------------------- Classes -------------------------
class Player (pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        #-- caracteristicas do personagem -- #
        self.name = None
        self.foto = None
        self.vidas = 10
        self.morte = False
        self.pontos = 0
        self.x_inicial = None
        self.y_inicial = None
        self.direcaoH = 0
        self.direcaoV = 0

        self.movimentos = True
        self.coluna = None
        self.linha = None
        self.frames = None
        self.frame_atual = 0
        self.img = None
        self.flipX = False
        self.flipeY = False
        #--- Inicializacao da bomba ---#
        self.bomba_coluna = None
        self.bomba_linha = None
        self.bomba_x = None
        self.bomba_y = None
        self.bomba_frames = None
        self.bomba_frame_atual = 0
        self.bomba_max_frames = 0
        self.bomba_img = None
        self.bombax_inicial = 0
        self.bombay_inicial = 0
        self.capturaPosicao = True
        self.detonador = False
        self.tempoDetonador = 3
        self.contDetonador = 0
        self.bombaAtiva = False
        self.listaExplosoes = []

    #----- metodos para as acoes -----#
    def drawAnimacao(self, scr):
        self.frames.sort()

        if self.direcaoH != 0:
            if self.name == "Player 1":
                self.frames = glob.glob("spr_bomber_direita*.png")
            else:
                self.frames = glob.glob("spr_bomber2_direita*.png")

            self.max_frames = len(self.frames)-1
            if self.frame_atual < self.max_frames:
                print ("max_frames: ", self.max_frames)
                self.frame_atual += 1
            else:
                self.frame_atual = 0

        if self.direcaoV > 0:
            if self.name == "Player 1":
                self.frames = glob.glob("spr_bomber_baixo*.png")
            else:
                self.frames = glob.glob("spr_bomber2_baixo*.png")
            self.max_frames = len(self.frames)-1
            if self.frame_atual < self.max_frames:
                self.frame_atual += 1
            else:
                self.frame_atual = 0

        if self.direcaoV < 0:
            if self.name == "Player 1":
                self.frames = glob.glob("spr_bomber_cima*.png")
            else:
                self.frames = glob.glob("spr_bomber2_cima*.png")
            self.max_frames = len(self.frames)-1
            if self.frame_atual < self.max_frames:
                self.frame_atual += 1
            else:
                self.frame_atual = 0
        scr.blit(self.img, (self.rect.x, self.rect.y))
        self.img = pygame.image.load(self.frames[self.frame_atual])

        if self.flipX == True:
            self.img = pygame.transform.flip(self.img, 1, 0)

    def move(self, dirX, dirY):

        if dirX != 0:
            self.aceleracao(dirX, 0)
            self.direcaoH = dirX
        else:
            self.direcaoH = 0

        if dirY != 0:
            self.aceleracao(0, dirY)
            self.direcaoH = dirY
        else:
            self.direcaoV = 0

    def aceleracao(self, dirX, dirY):
        self.rect.x += dirX
        self.rect.y += dirY

        tamanhoLista = len(listaRects)
        for i in range ((tamanhoLista)-1):
            rectAtual = listaRects[i]
            if self.rect.colliderect(rectAtual):
                if dirX > 0:
                    self.rect.right = rectAtual.left
                if dirX < 0:
                    self.rect.left = rectAtual.right
                if dirY > 0:
                    self.rect.bottom = rectAtual.top
                if dirY < 0:
                    self.rect.top = rectAtual.bottom

    def acionarBomba(self, scr, player):
        global listaExplosoes, perderVidas
        if player.detonador == True:
            player.bombaAtiva = True
            if player.bombaAtiva == True:
                player.contDetonador -= 1
                player.bomba_frames = glob.glob("spr_bomba_ativa*.png")
                player.bomba_img = pygame.image.load(player.bomba_frames[0])
                player.bomba_rect = player.bomba_img.get_rect()
                if player.contDetonador > 10:
                    if player.capturaPosicao == True:
                        player.bombax_inicial = player.rect.x
                        player.bombay_inicial = player.rect.y
                        comprimento = scr.get_width()/15
                        altura = scr.get_height()/13
                        player.bomba_coluna = int(player.bombax_inicial/(comprimento))
                        player.bomba_linha = int(player.bombay_inicial/(altura))

                        player.bombax = player.bomba_coluna*comprimento
                        player.bombay = player.bomba_linha*altura
                        player.capturaPosicao = False

                    player.bomba_frames.sort()
                    player.bomba_frames = glob.glob("spr_bomba_ativa*.png")
                    player.bomba_max_frames = len(player.bomba_frames)-1
                    if player.bomba_frame_atual < player.bomba_max_frames:
                        player.bomba_frame_atual += 1
                    else:
                        player.bomba_frame_atual = 0
                    player.bomba_img = pygame.image.load(player.bomba_frames[player.bomba_frame_atual])
                    scr.blit(player.bomba_img, (player.bombax, player.bombay))
                if player.contDetonador == 0:
                    player.detonador = False
                    player.bombaAtiva = False
                    player.capturaPosicao = True
                    player1.tamanhoListaExplosoes = len(player1.listaExplosoes)
                    player2.tamanhoListaExplosoes = len(player2.listaExplosoes)

                    for i in range((player2.tamanhoListaExplosoes)-1):  # saber se o player 1 colidiu com a bomba do player 2
                        if player1.rect.colliderect(player2.listaExplosoes[i]):
                            player1.vidas -= 1
                            print ("vidas player 1: ", player1.vidas)
                            player.morte = True
                            player1.rect.x = 120
                            player1.rect.y = 90
                            player2.listaExplosoes = []
                            break  # break para parar o for e a busca por possiveis colisoes e evitar bug do list out of range   #

                    for i in range ((player1.tamanhoListaExplosoes)-1): # saber se o player 2 colidiu com a bomba do player 1
                        if player2.rect.colliderect(player1.listaExplosoes[i]):
                                player2.vidas -= 1
                                print ("vidas player 2: ", player2.vidas)
                                player1.listaExplosoes = []
                                player.morte = True
                                player2.rect.x = 900
                                player2.rect.y = 560
                                break #break para parar o for e a busca por possiveis colisoes e evitar bug do list out of range

                    player1.tamanhoListaExplosoes = len(player1.listaExplosoes)
                    player2.tamanhoListaExplosoes = len(player2.listaExplosoes)

                    for i in range((player1.tamanhoListaExplosoes)-1): # saber se o player 2 colidiu com a propria bomba
                        if player1.rect.colliderect(player1.listaExplosoes[i]):
                            player1.vidas -= 1
                            print ("vidas player 1: ", player1.vidas)
                            player.morte = True
                            player1.rect.x = 120
                            player1.rect.y = 90
                            player1.listaExplosoes = []
                            break #break para parar o for e a busca por possiveis colisoes e evitar bug do list out of range

                    for i in range((player2.tamanhoListaExplosoes)-1): # testar se o player 2 colidiu com a propria bomba
                        if player2.rect.colliderect(player2.listaExplosoes[i]):
                            player2.vidas -= 1
                            print ("vidas player 1: ", player1.vidas)
                            player.morte = True
                            player2.rect.x = 900
                            player2.rect.y = 560
                            player2.listaExplosoes = []
                            break

    def drawExplosao(self, scr):
        global calculaColisoes, listaRects

        imgExplosao = {"meio": pygame.image.load('spr_explosao_meio0.png'),"vertical": pygame.image.load("spr_explosao_cima0.png").convert_alpha(),"horizontal": pygame.image.load("spr_explosao_direita0.png").convert_alpha()}
        if self.bombaAtiva == True:
            if self.contDetonador < 10:
                for linha in range (13):
                    for coluna in range(15):
                        if cenario[self.bomba_linha][self.bomba_coluna+1] == 0:
                            explosaoDireita = imgExplosao["horizontal"]
                            explosaoDireita = pygame.transform.rotate(explosaoDireita, 180)
                            scr.blit(explosaoDireita, (self.bombax+76, self.bombay))
                            explosaoHorizontalRect = Rect((self.bombax+76, self.bombay), (70, 52))
                            self.listaExplosoes.append(explosaoHorizontalRect)
                        if cenario[self.bomba_linha][self.bomba_coluna-1] == 0:
                            explosaoEsquerda = imgExplosao["horizontal"]
                            scr.blit(explosaoEsquerda, (self.bombax-80, self.bombay))
                            explosaoHorizontalRect = Rect((self.bombax-76, self.bombay), (70, 52))
                            self.listaExplosoes.append(explosaoHorizontalRect)

                        if cenario[self.bomba_linha - 1][self.bomba_coluna] == 0:
                            scr.blit(imgExplosao["vertical"], (self.bombax, self.bombay-56))
                            explosaoVerticalRect = Rect((self.bombax, self.bombay-56), (70, 70))
                            self.listaExplosoes.append(explosaoVerticalRect)
                        if cenario[self.bomba_linha +1][self.bomba_coluna] == 0:
                            imgBaixo = imgExplosao["vertical"]
                            imgBaixo = pygame.transform.rotate(imgBaixo, 180)
                            scr.blit(imgBaixo, (self.bombax, self.bombay+54))
                            explosaoVerticalRect = Rect((self.bombax, self.bombay+56), (60, 60))
                            self.listaExplosoes.append(explosaoVerticalRect)

                        if cenario[self.bomba_linha][self.bomba_coluna+1] == 2:
                            cenario[self.bomba_linha][self.bomba_coluna+1] = 0
                        if cenario[self.bomba_linha][self.bomba_coluna-1] == 2:
                            cenario[self.bomba_linha][self.bomba_coluna-1] = 0
                        if cenario[self.bomba_linha+1][self.bomba_coluna] == 2:
                            cenario[self.bomba_linha+1][self.bomba_coluna] = 0
                        if cenario[self.bomba_linha-1][self.bomba_coluna] == 0:
                            cenario[self.bomba_linha-1][self.bomba_coluna] = 0

                scr.blit(imgExplosao["meio"], (self.bombax, self.bombay))
                explosaoMeioRect = Rect((self.bombax, self.bombay), (60, 50))
                self.listaExplosoes.append(explosaoMeioRect)
                calculaColisoes = True
                if calculaColisoes == True:
                    #calculaColisoes  = False
                    listaRects = []
                    paredeCenario1.colisaoCenario(screen)
                    #print ("calcula colisao: ", calculaColisoes)

    def drawTexto(self, scr, cor, x, y, tamanho, nomeFont, texto, imgVidas, xvida, yvida, foto, xfoto, yfoto):
        if imgVidas == True:
            scr.blit(sprVidas, (xvida, yvida))

        if foto == True:
            scr.blit(self.foto, (xfoto, yfoto))
        texto = str(texto)
        fonte = pygame.font.SysFont(nomeFont,tamanho,False,False)
        texto = fonte.render(texto, True, cor)
        scr.blit(texto, (x, y))

#---- classe para as caracteristicas do cenario --- #
class Cenario(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.x = None
        self.y = None
        self.comprimento = None
        self.altura = None
        self.cenarioImagens = {"parede1" :pygame.image.load("spr_parede1.png"), "paredeQuebravel1": pygame.image.load("spr_parede_quebravel1.png")}
        self.img = self.cenarioImagens["parede1"]
        self.rect = self.img.get_rect()
    # ----- metodos para pintar o cenario e calcular as ares de colisao --- #
    def pintaCenario(self, scr):
        global musica2
        for linha in range(13):
            for coluna in range(15):
                self.comprimento = scr.get_width()/15
                self.altura = scr.get_height()/13
                self.x = self.comprimento*coluna
                self.y = self.altura*linha
                if cenario[linha][coluna] == 0:
                    cor = (50, 50, 50)
                    r = Rect(self.x, self.y, self.comprimento, self.altura)
                    pygame.draw.rect(scr, cor, r)
                if cenario[linha][coluna] == 1:
                    scr.blit(self.cenarioImagens["parede1"], (self.x, self.y))
                if cenario[linha][coluna] == 2:
                    scr.blit(self.cenarioImagens["paredeQuebravel1"], (self.x, self.y))
                    #scr.blit(imgTeste, (200, 220))

    def colisaoCenario(self, scr):
            global listaRects
            for linha in range(13):
                for coluna in range(15):
                    self.comprimento = scr.get_width()/15
                    self.altura = scr.get_height()/13
                    self.x = self.comprimento*coluna
                    self.y = self.altura*linha
                    if cenario[linha][coluna] == 1:
                        r = Rect(self.x, self.y, self.comprimento, self.altura)
                        listaRects.append(r)
                    if cenario[linha][coluna] == 2:
                        r = Rect(self.x, self.y, self.comprimento, self.altura)
                        listaRects.append(r)

#--------------------------------------- Unica funcao que nao pertence a classe personagem nem cenario ------------------------#
def mostrarVencedor(scr):
    global vencedor
    tamanho = 100
    xx = 0
    yy = scr.get_height()/2
    if player1.vidas < 1 and player2.vidas > 0:
        texto = "O vencendor foi o Player 2!!!"
        player1.drawTexto(screen,(0, 0, 0),  xx+4, yy+4, tamanho, 'DotumChe',  texto, False, 0, 0, True, xx+500, yy-100,)
        player1.drawTexto(screen,(255, 255, 255),  xx, yy, tamanho, 'DotumChe',  texto, False, 0, 0, False, 0, 0,)
    else:
        empate = True
        vencedor = True
    if player2.vidas < 1 and player1.vidas > 0:
        texto = "O vencendor foi o Player 1!!!"
        player1.drawTexto(screen,(0, 0, 0),  xx+4, yy+4, tamanho, 'DotumChe',  texto, False, 0, 0, True, xx+500, yy-100,)
        player1.drawTexto(screen,(255, 255, 255),  xx, yy, tamanho, 'DotumChe',  texto, False, 0, 0, False, 0, 0)
        vencedor = True
    else:
        empate = True

    if player1.vidas < 1 and player2.vidas < 1 and  empate == True:
        texto = "Empate !!!"
        player1.drawTexto(screen,(0, 0, 0),  xx+400, yy+4, tamanho, 'DotumChe',  texto, False, 0, 0, False, 0, 0)
        player1.drawTexto(screen,(255, 255, 255),  xx+400, yy, tamanho, 'DotumChe',  texto, False, 0, 0, False, 0, 0)

def chamaMenu (scr,):
    global menu
    menuImgScale = pygame.transform.scale(menuInfos["img"], (screenSize))
    scr.blit(menuImgScale, (0, 0))
    player1.drawTexto(screen,(255, 255, 255), menuInfos["texto1"][2], menuInfos["texto1"][3], menuInfos["texto1"][1], 'Impact', menuInfos["texto1"][0], False, 0, 0, False, 0, 0)
    player1.drawTexto(screen,(255, 255, 255), menuInfos["texto2"][2], menuInfos["texto2"][3], menuInfos["texto2"][1], 'Impact', menuInfos["texto2"][0], False, 0, 0, False, 0, 0)

def alteraMenu(opc):
    global menu
    if opc == 1:
        menuInfos["texto1"][1] = 40
        menuInfos["texto2"][1] = 20
        menuInfos["texto2"][3] = 500

    if opc == 2:
        menuInfos["texto2"][1] = 50
        menuInfos["texto1"][1] = 20
        menuInfos["texto2"][3] = 480
#----------------------------------- variaveis globais -------------------------------------------------#
vencedor = False
listaRects = []
listaExplosoes = []
calculaColisoes = False
sprVidas = pygame.image.load("spr_vidas.png")
menuInfos  =  {"img": pygame.image.load("bomber_menu.png"), "texto1": ["Iniciar" , 20, 500, 450 ], "texto2": ["Sair", 20, 500, 500]}
menu = True
opcao = 1

musica1= pygame.mixer.music.load("464646.mp3")
musica2 = pygame.mixer.music.load("565656.mp3")
tocarMusica2 = False
#----------------------------------- Instanciando objetos -------------------------------------
#imgTeste = pygame.image.load("spr_guts_andando3.png")
paredeCenario1 = Cenario()
paredeCenario1.colisaoCenario(screen)

player1 = Player()
player1.name = "Player 1"
player1.movimento = 0
player1.frames = glob.glob("spr_bomber_direita*.png")
player1.img = pygame.image.load(player1.frames[0])
player1.rect = player1.img.get_rect()
player1.rect.x = int(player1.img.get_width() + (player1.img.get_width()/2))
player1.rect.y = int(player1.img.get_height() + (player1.img.get_height()/2))
player1.x_inicial = 140
player1.y_inicial = 90
player1.rect.x = 140
player1.rect.y = 90
# levar o x inicial para o meio --- player1.rect.x = int(player1.img.get_width() + (player1.img.get_width()/2))
# levar o y inicial para o meio --- player1.rect.y = int(player1.img.get_height() + (player1.img.get_height()/2))
player1.bomba_frames = glob.glob("spr_bomba_ativa*.png")
player1.bomba_img = pygame.image.load(player1.bomba_frames[0])
player1.foto = pygame.image.load("spr_bomber_foto.png")

player2 = Player()
player2.name = "Player 2"
player2.frames = glob.glob("spr_bomber2_direita*.png")
player2.img = pygame.image.load(player2.frames[0])
player2.rect = player2.img.get_rect()
player2.x_inicial = 900
player2.y_inicial = 560
player2.rect.x = 900
player2.rect.y = 560
player2.flipX = True
player2.bomba_frames = glob.glob("spr_bomba_ativa*.png")
player2.bomba_img = pygame.image.load(player2.bomba_frames[0])
player2.foto = pygame.image.load("spr_bomber2_foto.png")

pygame.mixer.music.play(1,-3)
#---------------------------------Loop do game ----------------------------------
while True:
    clock.tick(FPS)
    pygame.display.update()
    screen.fill((50, 50, 50))

    for e in pygame.event.get():
        if e.type == QUIT:
            print ("player rect : ", player1.rect.x, player1.rect.y)
            exit()

    if menu == True:
        chamaMenu(screen)
        if pygame.key.get_pressed()[K_DOWN]:
            opcao = 2
        if pygame.key.get_pressed()[K_UP]:
            opcao = 1
        alteraMenu(opcao)

        if pygame.key.get_pressed()[K_SPACE] and opcao == 1:
            musica1 = pygame.mixer.music.fadeout(1,)
            tocarMusica2 = True
            if tocarMusica2 == True:
                musica2 = pygame.mixer.music.play(1,-1)
                tocarMusica2 = False
            menu = False
        if pygame.key.get_pressed()[K_SPACE] and opcao == 2:
            exit()
    else:

        paredeCenario1.pintaCenario(screen)
        #-----------Chamando funcoes do player1----------
        player1.drawTexto(screen, (0, 0, 0), 26, 75, 20, 'Impact', player1.vidas, True, 0, 48, True, 0, 0)
        player1.acionarBomba(screen, player1)
        player1.drawAnimacao(screen)
        player1.drawExplosao(screen)
        #-----------Chamando funcoes do player2----------
        player2.drawTexto(screen, (0, 0, 0), 1095, 75, 20, 'Impact', player2.vidas, True, 1070, 48, True, 1064, 0)
        player2.acionarBomba(screen, player2)
        player2.drawAnimacao(screen)
        player2.drawExplosao(screen)

        #---------------------------comandos player 1 ---------------------------#
        key = pygame.key.get_pressed()
        if key[pygame.K_LEFT]:
            player1.move(-5, 0)
            player1.flipX = True
            player1.direcaoH = -1
        else:
            player1.direcaoH = 0

        if key[pygame.K_RIGHT]:
            player1.move(5, 0)
            player1.flipX = False
        else:
            player1.direcaoH = 0

        if key[pygame.K_UP]:
            player1.move(0, -5)

        else:
            player1.direcaoV = 0

        if key[pygame.K_DOWN]:
            player1.move(0, 5)
        else:
            player1.direcaoV = 0

        if key[pygame.K_INSERT]:
            if player1.bombaAtiva == False:
                player1.contDetonador = player2.tempoDetonador*FPS
                player1.detonador = True

        player1.direcaoH = pygame.key.get_pressed()[K_RIGHT] + (-pygame.key.get_pressed()[K_LEFT])
        player1.direcaoV = pygame.key.get_pressed()[K_DOWN] + (-pygame.key.get_pressed()[K_UP])

        #-------------------- player 2 comandos ------------------------------#
        key = pygame.key.get_pressed()
        if key[pygame.K_a]:
            player2.move(-5, 0)
            player2.flipX = True
            player2.direcaoH = -1
        else:
            player2.direcaoH = 0

        if key[pygame.K_d]:
            player2.move(5, 0)
            player2.flipX = False
        else:
            player2.direcaoH = 0

        if key[pygame.K_w]:
            player2.move(0, -5)

        else:
            player2.direcaoV = 0

        if key[pygame.K_s]:
            player2.move(0, 5)
        else:
            player2.direcaoV = 0

        if key[pygame.K_b]:
            if player2.bombaAtiva == False:
                player2.contDetonador = player2.tempoDetonador*FPS
                player2.detonador = True

        player2.direcaoH = pygame.key.get_pressed()[K_d] + (-pygame.key.get_pressed()[K_a])
        player2.direcaoV = pygame.key.get_pressed()[K_s] + (-pygame.key.get_pressed()[K_w])

        mostrarVencedor(screen)

    pygame.display.update()

